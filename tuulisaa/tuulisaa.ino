
#include <UIPEthernet.h> // Used for Ethernet
#include <LiquidCrystal.h>
#include <PubSubClient.h>
#include <math.h>
// **** ETHERNET SETTING ****
// Arduino Uno pins: 10 = CS, 11 = MOSI, 12 = MISO, 13 = SCK
// Ethernet MAC address - must be unique on your network - MAC Reads T4A001 in hex (unique in your network)
byte mac[] = { 0x54, 0x34, 0x41, 0x32, 0x34, 0x54 };
// For the rest we use DHCP (IP address and such)
LiquidCrystal lcd(7, 6, 5, 4, 3, 9);
EthernetClient ethclient;
EthernetUDP udp;
byte server[] = { 10, 0, 0, 1 };
PubSubClient client(server, 1883, callback, ethclient);
//int  interval = 250; // Wait between dumps
unsigned long aika = 0;
unsigned long ero= 0;
volatile unsigned int frq = 0;
float tulos = 0;
unsigned int mittaus_aika = 0;
unsigned int des;
unsigned int lasku = 0;
char temp[10];
char kello[8];
//signed long vanha;

void callback(char* topic, byte* payload, unsigned int length) {
	// handle message arrived
}


void setup() {
	pinMode(8, OUTPUT);
	digitalWrite(8, HIGH);
	lcd.begin(16, 2);
	//Serial.begin(9600);
	print(2, 0, 0);
	
	Ethernet.begin(mac);
	/*	Serial.print("IP Address        : ");
	Serial.println(Ethernet.localIP());
	Serial.print("Subnet Mask       : ");
	Serial.println(Ethernet.subnetMask());
	Serial.print("Default Gateway IP: ");
	Serial.println(Ethernet.gatewayIP());
	Serial.print("DNS Server IP     : ");
	Serial.println(Ethernet.dnsServerIP());
	*/

	//unsigned long unixTime = ntpUnixTime(udp);
	//Serial.println(unixTime);
	print(3, 0, 0);
	aika = ntpUnixTime(udp);
	//aika = 0;
	attachInterrupt(0, pulssi, FALLING);
}

void loop() {
	client.loop();
	if (!client.connected())
	{
		client.connect("arduinoClient5");
	}
	//ero += millis() - vanha;
	//vanha = millis();
	if (millis()-ero > 1000)
	{
		ero = millis();
		aika+=(ero/1000);
		print(6, aika, tulos);
		mittaus_aika = millis() - lasku;
		lasku = millis();
		tulos = nopeuslasku(frq, mittaus_aika);
		//ero -= 1000;
		if (aika % 6 == 5)
		{	
			if (!ethclient.connected())
			{
				Enc28J60.init(mac);
			}
			tulos = tulos * 100;
			des = (int)tulos % 100;
			tulos = tulos / 100;
			//Serial.println(tulos);
			frq = 0;
			sprintf(temp, "%d.%02d", (int)tulos,des);
			//dtostrf(tulos, 6, 2, temp);
				if (!client.publish("/sensors/tnop", temp))
				{
						/*digitalWrite(8, LOW);
						delay(10);
						digitalWrite(8, HIGH);
						delay(10);*/
						//Enc28J60.init(mac);
						//Ethernet.begin(mac, Ethernet.localIP());
				}
			
			
		}
		Ethernet.maintain();
	}
}


/*
* � Francesco Potort� 2013 - GPLv3 - Revision: 1.13
*
* Send an NTP packet and wait for the response, return the Unix time
*
* To lower the memory footprint, no buffers are allocated for sending
* and receiving the NTP packets.  Four bytes of memory are allocated
* for transmision, the rest is random garbage collected from the data
* memory segment, and the received packet is read one byte at a time.
* The Unix time is returned, that is, seconds from 1970-01-01T00:00.
*/
unsigned long inline ntpUnixTime(UDP &udp)
{
	static int udpInited = udp.begin(123); // open socket on arbitrary port

	const char timeServer[] = "10.0.0.1";  // NTP server

	// Only the first four bytes of an outgoing NTP packet need to be set
	// appropriately, the rest can be whatever.
	const int ntpFirstFourBytes = 0xEC0600E3; // NTP request header

	// Fail if WiFiUdp.begin() could not init a socket
	if (!udpInited)
		return 0;

	// Clear received data from possible stray received packets
	udp.flush();

	// Send an NTP request
	if (!(udp.beginPacket(timeServer, 123) // 123 is the NTP port
		&& udp.write((byte *)&ntpFirstFourBytes, 48) == 48
		&& udp.endPacket()))
		return 0;				// sending request failed

	// Wait for response; check every pollIntv ms up to maxPoll times
	const int pollIntv = 150;		// poll every this many ms
	const byte maxPoll = 15;		// poll up to this many times
	int pktLen;				// received packet length
	for (byte i = 0; i<maxPoll; i++) {
		if ((pktLen = udp.parsePacket()) == 48)
			break;
		delay(pollIntv);
	}
	if (pktLen != 48)
		return 0;				// no correct packet received

	// Read and discard the first useless bytes
	// Set useless to 32 for speed; set to 40 for accuracy.
	const byte useless = 40;
	for (byte i = 0; i < useless; ++i)
		udp.read();

	// Read the integer part of sending time
	unsigned long time = udp.read();	// NTP time
	for (byte i = 1; i < 4; i++)
		time = time << 8 | udp.read();

	// Round to the nearest second if we want accuracy
	// The fractionary part is the next byte divided by 256: if it is
	// greater than 500ms we round to the next second; we also account
	// for an assumed network delay of 50ms, and (0.5-0.05)*256=115;
	// additionally, we account for how much we delayed reading the packet
	// since its arrival, which we assume on average to be pollIntv/2.
	time += (udp.read() > 115 - pollIntv / 8);
	// Discard the rest of the packet
	udp.flush();
	return time - 2208988800ul;		// convert NTP time to Unix time
}


bool print(uint8_t daatta, unsigned long time, float result){
	uint8_t h, m, s;
	s = aika % 60;
	m = (aika / 60) % 60;
	h = (aika / 3600) % 24;
	lcd.clear();
	sprintf(kello, "%02d:%02d:%02d", h, m, s);
	if (daatta == 0){ // K�YNNISTYS RUUTU
		lcd.setCursor(0, 0);
		lcd.print("Teretulemast    ");
		lcd.setCursor(0, 1);
		lcd.print("                ");
	}

	if (daatta == 1){ // TEST MODE ENABLED
		lcd.setCursor(0, 0);
		lcd.print("Testi moodi     ");
		lcd.setCursor(0, 1);
		lcd.print("                ");
	}

	else if (daatta == 2){ // ODOTELLAAN IP:T�
		lcd.setCursor(0, 0);
		lcd.print("Odotetaan IP:ta ");
		lcd.setCursor(0, 1);
		lcd.print("                ");
	}

	else if (daatta == 3){ // IP SAATU, NTP PYYNT�

		lcd.setCursor(0, 0);
		lcd.print("IP=");
		lcd.print(Ethernet.localIP());
		lcd.setCursor(0, 1);
		lcd.print("NTP-kysely      ");
	}

	else if (daatta == 4){ // IP SAATU, ODOTETAAN AIKAA

		lcd.setCursor(0, 0);
		lcd.print("IP=");
		lcd.print(Ethernet.localIP());


		lcd.setCursor(0, 1);
		lcd.print("Odotetaan aikaa ");
	}

	else if (daatta == 5){ // DHCP EP�ONNISTUI
		lcd.setCursor(0, 0);
		lcd.print("DHCP ep�onnistui");
		lcd.setCursor(0, 1);
		lcd.print("                ");
	}

	else if (daatta == 6){ // IP YMS SAATU, MITTATULOKSET ESIIN!
		
		lcd.setCursor(0, 0);
		lcd.print("Tnop 1          ");
		//result = 2000000000;
		lcd.setCursor(0, 1);
		lcd.print(result);
		//time = 12345678;
		lcd.setCursor(8, 0);
		lcd.print(kello);
		lcd.setCursor(13, 1);
		//lcd.print(apu_sek);
	}
}

void pulssi()
{
	frq++;
}

float nopeuslasku(unsigned int pulssit, float aika)
{
	float nopeus = 0;
		nopeus = 5.9862 * log(pulssit / (aika/1000)) + 4.2687;
		if (nopeus <= 0)
		{
			return 0;
		}
		else
		{
			return nopeus;
		}
	}