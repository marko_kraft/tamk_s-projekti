# README #

Muunneltu koodi eri säämittauksiin. Koodia itsessään ei ole juuri kommmentoitu, mutta sen pitäisi olla melko helppo lukuista. Parhaiten kommentoitu ja läpiluettu on sade.ino tiedosto. 

### Sisältyvät koodit ###

* Tuulen noupeus
* Tuulen suunta
* Lämpötila
* Sademäärä

### Toiminta ###

* Kirjastojen esittely
* Ethernet alustus
* Näytön alustus
* Globaalien esittely
* Mqtt yhteys (10.0.0.1)
* Ntp ajan haku lokaalista(10.0.0.1)
* Mittaus koodit
* Ajan päivitys näytölle
* Lähetys haluttuun kanavaan tietyin väliajoin

### TODO ###

* Koodien siistintä / yhden mukaistaminen
* Turhien debug koodien poisto
* Other guidelines

### Debug ###

* Laita sarjoportti tulosteita ja kytke koneeseen (korkein baudi)
* Laita päälle koodissa esiintyviä virheen korjauksia esim shieldin reset nastan alhaalla käyttö

### Kirjastot ###
* ntruchsess/arduino_uip https://github.com/ntruchsess/arduino_uip/tree/fix_errata12 (HUOM! brach fix_errata12)
* knolleary/pubsubclient https://github.com/knolleary/pubsubclient

### Keskustelut ###
* https://github.com/ntruchsess/arduino_uip/pull/103
* http://forum.mysensors.org/topic/536/problems-with-enc28j60-losing-connection-freezing-using-uipethernet-or-ethershield-read-this
* 